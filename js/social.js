var twitter_intent_url = 'https://twitter.com/intent/tweet?lang=es&related=redfria2&hashtags=redfria2,DrPingu';
var twitter_post_url = 'https://api.twitter.com/1.1/statuses/update.json';
var f_score_text = function(level, score) {
  return 'He llegado al nivel ' + level + ' en Doctor Pingu con ' + score + ' puntos';
}

function fill_twitter(level, score, url) {
  if( typeof(url) === 'undefined' ) url=document.URL;
  $('#twitter-score-button').attr('href', twitter_intent_url + '&url=' + encodeURIComponent(url) + '&text=' + encodeURIComponent(f_score_text(level, score)) );
  $('#twitter-score-button').off('click');
  $('#twitter-score-button').on('click', function(){
    return true;
    /*
    var _toret = false;
    $.ajax({
      url: twitter_post_url,
      type: 'POST',
      async: false,
      data: {
        status: encodeURIComponent(f_score_text(level, score) + ' #redfria2 #DrPingu ' + url),
      },
      success: function(e) {
      },
      error: function(e) {
        //window.open( twitter_intent_url + '&url=' + encodeURIComponent(url) + '&text=' + encodeURIComponent(f_score_text(level, score)), '_blank');
        _toret = true;
      },
      complete: function(e) {
      },
    });
    return _toret;
    */
  });
}

